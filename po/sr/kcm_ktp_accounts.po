# Translation of kcm_ktp_accounts.po into Serbian.
# Mladen Pejakovic <pejakm@gmail.com>, 2012.
# Chusslove Illich <caslav.ilic@gmx.net>, 2012, 2013, 2016.
msgid ""
msgstr ""
"Project-Id-Version: kcm_ktp_accounts\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 20:16+0100\n"
"PO-Revision-Date: 2016-03-20 15:25+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Младен Пејаковић"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "pejakm@gmail.com"

# >> @title:window
#: account-identity-dialog.cpp:42
#, kde-format
msgid "Edit Account Identity"
msgstr "Уређивање идентитета налога"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: account-identity-dialog.ui:40
#, kde-format
msgid "Nickname:"
msgstr "Надимак:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: account-identity-dialog.ui:50
#, kde-format
msgid "Account:"
msgstr "Налог:"

#. i18n: ectx: property (text), widget (AvatarButton, accountAvatar)
#: account-identity-dialog.ui:87
#, kde-format
msgid "..."
msgstr "..."

#: accounts-list-delegate.cpp:70
#, kde-format
msgid "Change account icon"
msgstr "Промени иконицу налога"

#: accounts-list-delegate.cpp:71
#, kde-format
msgid ""
"This button allows you to change the icon for your account.<br />This icon "
"is just used locally on your computer, your contacts will not be able to see "
"it."
msgstr ""
"<p>Овим дугметом можете променити иконицу свог налога.</p><p>Иконица се "
"користи само локално на овом рачунару, ваши контакти неће моћи да је виде.</"
"p>"

#: accounts-list-delegate.cpp:79
#, kde-format
msgid "Change account display name"
msgstr "Промени приказно име налога"

#: accounts-list-delegate.cpp:80
#, kde-format
msgid ""
"This button allows you to change the display name for your account.<br />The "
"display name is an alias for your account and is just used locally on your "
"computer, your contacts will not be able to see it."
msgstr ""
"<p>Овим дугметом можете променити приказно име свог налога.</p><p>Приказно "
"име је псеудоним за ваш налог и користи се само локално на овом рачунару, "
"ваши контакти неће моћи да га виде.</p>"

#: accounts-list-delegate.cpp:134
#, kde-format
msgid "Click checkbox to enable"
msgstr "Кликните на кућицу да активирате"

#: accounts-list-delegate.cpp:144
#, kde-format
msgid "Disable account"
msgstr "Деактивирај налог"

#: accounts-list-delegate.cpp:147
#, kde-format
msgid "Enable account"
msgstr "Активирај налог"

#: add-account-assistant.cpp:95 add-account-assistant.cpp:96
#, kde-format
msgid "Step 1: Select an Instant Messaging Network."
msgstr "Корак 1: Изаберите брзогласничку мрежу."

#: add-account-assistant.cpp:120
#, kde-format
msgid "Step 2: Fill in the required Parameters."
msgstr "Корак 2: Попуните захтеване параметре."

#: add-account-assistant.cpp:172
#, kde-format
msgid ""
"To connect to this IM network, you need to install additional plugins. "
"Please install the telepathy-haze and telepathy-gabble packages using your "
"package manager."
msgstr ""
"Да бисте се повезали на ову мрежу требају вам додатни прикључци. "
"Инсталирајте пакете telepathy-haze и telepathy-gabble помоћу менаџера пакета."

#: add-account-assistant.cpp:172
#, kde-format
msgid "Missing Telepathy Connection Manager"
msgstr "Недостаје телепатијски менаџер повезивања"

#: add-account-assistant.cpp:208 add-account-assistant.cpp:257
#: salut-enabler.cpp:243
#, kde-format
msgid "Failed to create account"
msgstr "Прављење налога није успело"

#: add-account-assistant.cpp:258 salut-enabler.cpp:244
#, kde-format
msgid "Possibly not all required fields are valid"
msgstr "Могуће је да нека захтевана поља нису исправна."

#: add-account-assistant.cpp:267 salut-enabler.cpp:253
#, kde-format
msgid "Something went wrong with Telepathy"
msgstr "Нешто није у реду са Телепатијом."

#: avatar-button.cpp:54
#, kde-format
msgid "Load from file..."
msgstr "Учитај из фајла..."

#: avatar-button.cpp:55
#, kde-format
msgid "Clear Avatar"
msgstr "Очисти аватар"

#: avatar-button.cpp:106
#, kde-format
msgid "Please choose your avatar"
msgstr "Изаберите свој аватар."

#: avatar-button.cpp:155
#, kde-format
msgid "Failed to load avatar."
msgstr "Учитавање аватара није успело."

# >> @title:window
#: edit-display-name-button.cpp:61
#, kde-format
msgid "Edit Display Name"
msgstr "Уређивање приказног имена"

#: edit-display-name-button.cpp:70
#, kde-format
msgid "Choose a new display name for your account"
msgstr "Изаберите ново приказно име за свој налог"

#: edit-display-name-button.cpp:77 edit-display-name-button.cpp:87
#, kde-format
msgid ""
"A display name is your local alias for the account, only you will see it."
msgstr ""
"Приказно име је ваш локални псеудоним за налог, само га ви можете видети."

#: edit-display-name-button.cpp:86
#, kde-format
msgid "New display name"
msgstr "Ново приказно име"

#: kcm-telepathy-accounts.cpp:76
#, kde-format
msgid "telepathy_accounts"
msgstr "Телепатијски налози"

#: kcm-telepathy-accounts.cpp:76
#, kde-format
msgid "Instant Messaging and VOIP Accounts"
msgstr "Брзогласнички и ВоИП налози"

#: kcm-telepathy-accounts.cpp:78
#, kde-format
msgid "George Goldberg"
msgstr "Џорџ Голдберг"

#: kcm-telepathy-accounts.cpp:78 kcm-telepathy-accounts.cpp:79
#: kcm-telepathy-accounts.cpp:80 kcm-telepathy-accounts.cpp:81
#: kcm-telepathy-accounts.cpp:82 kcm-telepathy-accounts.cpp:83
#: kcm-telepathy-accounts.cpp:84
#, kde-format
msgid "Developer"
msgstr "Програмер"

#: kcm-telepathy-accounts.cpp:79
#, kde-format
msgid "David Edmundson"
msgstr "Дејвид Едмундсон"

#: kcm-telepathy-accounts.cpp:80
#, kde-format
msgid "Dominik Schmidt"
msgstr "Доминик Шмит"

#: kcm-telepathy-accounts.cpp:81
#, kde-format
msgid "Thomas Richard"
msgstr "Томас Рихард"

#: kcm-telepathy-accounts.cpp:82
#, kde-format
msgid "Florian Reinhard"
msgstr "Флоријан Рајнхард"

#: kcm-telepathy-accounts.cpp:83
#, kde-format
msgid "Daniele E. Domenichelli"
msgstr "Данијеле Доменичели"

#: kcm-telepathy-accounts.cpp:84
#, kde-format
msgid "Martin Klapetek"
msgstr "Мартин Клапетек"

#: kcm-telepathy-accounts.cpp:266
#, kde-format
msgid ""
"We have found Kopete logs for this account. Do you want to import the logs "
"to KDE Telepathy?"
msgstr ""
"Нађени су Копетеови дневници за овај налог. Желите ли да их увезете у "
"Телепатију?"

#: kcm-telepathy-accounts.cpp:267
#, kde-format
msgid "Import Logs?"
msgstr "Увести дневнике?"

#: kcm-telepathy-accounts.cpp:268
#, kde-format
msgid "Import Logs"
msgstr "Увези дневнике"

#: kcm-telepathy-accounts.cpp:269
#, kde-format
msgid "Close"
msgstr "Затвори"

#: kcm-telepathy-accounts.cpp:276
#, kde-format
msgid "Importing logs..."
msgstr "Увозим дневнике..."

#: kcm-telepathy-accounts.cpp:294 kcm-telepathy-accounts.cpp:303
#, kde-format
msgid "Kopete Logs Import"
msgstr "Увоз Копетеових дневника"

#: kcm-telepathy-accounts.cpp:303
#, kde-format
msgid "Kopete logs successfully imported"
msgstr "Копетеови дневници успешно увезени"

#. i18n: ectx: property (text), widget (KPushButton, removeAccountButton)
#: kcm-telepathy-accounts.cpp:405 kcm-telepathy-accounts.cpp:406
#: main-widget.ui:184
#, kde-format
msgid "Remove Account"
msgstr "Уклони налог"

# >> @title:window
#: kcm-telepathy-accounts.cpp:409
#, kde-format
msgid "Remove conversations logs"
msgstr "Уклањање дневника̂ разговора"

#: kcm-telepathy-accounts.cpp:410
#, kde-format
msgid "Are you sure you want to remove the account \"%1\"?"
msgstr "Желите ли заиста да уклоните налог „%1“?"

#: kcm-telepathy-accounts.cpp:492
#, kde-format
msgid "Install telepathy-salut to enable"
msgstr "Инсталирајте telepathy-salut за активирање"

#: kcm-telepathy-accounts.cpp:534
#, kde-format
msgid ""
"Something went terribly wrong and the IM system could not be initialized.\n"
"It is likely your system is missing Telepathy Mission Control package.\n"
"Please install it and restart this module."
msgstr ""
"Нешто је пошло по злу и брзогласнички систем не може да се припреми.\n"
"Вероватно на систему није инсталиран пакет Телепатијско управљање мисијом.\n"
"Инсталирајте га и поново покрените овај модул."

#: KCMTelepathyAccounts/abstract-account-parameters-widget.cpp:59
#, kde-format
msgid "All mandatory fields must be filled"
msgstr "Сва обавезна поља морају бити попуњена"

#: KCMTelepathyAccounts/account-edit-widget.cpp:108
#, kde-format
msgid "Connect when wizard is finished"
msgstr "Повежи се када чаробњак заврши"

#: KCMTelepathyAccounts/account-edit-widget.cpp:219
#, kde-format
msgid "Advanced Options"
msgstr "Напредне опције"

#. i18n: ectx: property (text), widget (QPushButton, advancedButton)
#: KCMTelepathyAccounts/account-edit-widget.ui:62
#, kde-format
msgid "Advanced"
msgstr "Напредно"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:33
#, kde-format
msgid "Password"
msgstr "Лозинка"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:34
#, kde-format
msgid "Account"
msgstr "Налог"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:35
#, kde-format
msgid "Priority"
msgstr "приоритет"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:36
#, kde-format
msgid "Port"
msgstr "Порт"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:37
#, kde-format
msgid "Alias"
msgstr "Псеудоним"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:38
#, kde-format
msgid "Register new Account"
msgstr "Региструј нови налог"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:39
#, kde-format
msgid "Server Address"
msgstr "Адреса сервера"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:40
#, kde-format
msgid "Fallback STUN server address"
msgstr "Адреса одступног СТУН сервера"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:41
#, kde-format
msgid "Resource"
msgstr "Ресурс"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:42
#, kde-format
msgid "HTTPS Proxy Port"
msgstr "Порт ХТТПС проксија"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:43
#, kde-format
msgid "Require Encryption"
msgstr "Захтевај шифровање"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:44
#, kde-format
msgid "Old-style SSL support"
msgstr "Подршка за старовремски ССЛ"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:45
#, kde-format
msgid "Fallback STUN port"
msgstr "Одступни СТУН порт"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:46
#, kde-format
msgid "Fallback Conference Server Address"
msgstr "Адреса одступног конференцијског сервера"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:47
#, kde-format
msgid "Low Bandwidth Mode"
msgstr "Ускопојасни режим"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:48
#, kde-format
msgid "STUN Server Address"
msgstr "Адреса СТУН сервера"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:49
#, kde-format
msgid "STUN Port"
msgstr "СТУН порт"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:50
#, kde-format
msgid "Fallback SOCKS5 Proxy Addresses"
msgstr "Адресе одступних СОКС‑5 проксија"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:51
#, kde-format
msgid "HTTPS Proxy Server Address"
msgstr "Адресе ХТТП прокси сервера"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:52
#, kde-format
msgid "Ignore SSL Errors"
msgstr "Занемари ССЛ грешке"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:53
#, kde-format
msgid "Keepalive Interval"
msgstr "Период одржавања везе"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:56
#, kde-format
msgid "AOL Instant Messenger"
msgstr "АОЛ‑ов брзи гласник"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:57
#: KCMTelepathyAccounts/dictionary.cpp:63
#, kde-format
msgid "Skype"
msgstr "Скајп"

# >! UI context not clear
#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonFacebook)
#: KCMTelepathyAccounts/dictionary.cpp:58
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:124
#, kde-format
msgid "Facebook Chat"
msgstr "Фејсбуково ћаскање"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:59
#, kde-format
msgid "Gadu-Gadu"
msgstr "Гаду‑гаду"

# >! UI context not clear
#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonGTalk)
#: KCMTelepathyAccounts/dictionary.cpp:60
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:100
#, kde-format
msgid "Google Talk"
msgstr "Гугл‑разговора"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:61
#, kde-format
msgid "Novell Groupwise"
msgstr "Новелов групвајз"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:62
#, kde-format
msgid "ICQ"
msgstr "Ајсикју"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:64
#, kde-format
msgid "Internet Relay Chat"
msgstr "Ћаскање путем Интернета (ИРЦ)"

# >! UI context not clear
#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonJabber)
#: KCMTelepathyAccounts/dictionary.cpp:65
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:54
#, kde-format
msgid "Jabber/XMPP"
msgstr "Џабер/ИксМПП"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:66
#, kde-format
msgid "Bonjour/Salut"
msgstr "Бонжур/Салут"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:67
#, kde-format
msgid "MXit"
msgstr "Миксит"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:68
#, kde-format
msgid "Windows Live Messenger"
msgstr "Виндоуз лајв месенџер"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:69
#, kde-format
msgid "MySpaceIM"
msgstr "Мајспејсов ИМ"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:70
#, kde-format
msgid "Tencent QQ"
msgstr "Тенцентов ку‑ку"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:71
#, kde-format
msgid "IBM Lotus Sametime"
msgstr "ИБМ‑ов сејмтајм"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:72
#, kde-format
msgid "SILC"
msgstr "СИЛЦ"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:73
#, kde-format
msgid "Session Initiation Protocol (SIP)"
msgstr "Протокол започињања везе (СИП)"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:74
#, kde-format
msgid "GSM Telephony"
msgstr "ГСМ телефонија"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:75
#, kde-format
msgid "Trepia"
msgstr "Трепија"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:76
#, kde-format
msgid "Yahoo! Messenger"
msgstr "Јахуов месенџер"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:77
#, kde-format
msgid "Yahoo! Messenger Japan"
msgstr "Јахуов месенџер јапански"

# >! UI context not clear
#: KCMTelepathyAccounts/dictionary.cpp:78
#, kde-format
msgid "Zephyr"
msgstr "Зефир"

#: KCMTelepathyAccounts/parameter-edit-model.cpp:301
#, kde-format
msgid "Parameter \"<b>%1</b>\" is not valid."
msgstr "Параметар <b>%1</b> није добар."

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonKDETalk)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:78
#, kde-format
msgid "KDETalk"
msgstr "КДЕ разговор"

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonOthers)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:148
#, kde-format
msgid "All"
msgstr "Сви"

#. i18n: ectx: property (description), widget (QCommandLinkButton, buttonOthers)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:162
#, kde-format
msgid "AOL, Gadu-Gadu, Yahoo and more..."
msgstr "АОЛ, гаду‑гаду, Јаху и остали..."

#: KCMTelepathyAccounts/validated-line-edit.cpp:100
#, kde-format
msgid "This field is required"
msgstr "Ово поље је обавезно"

#: KCMTelepathyAccounts/validated-line-edit.cpp:121
#, kde-format
msgid "This field should not be empty"
msgstr "Ово поље не би требало да буде празно"

#: KCMTelepathyAccounts/validated-line-edit.cpp:125
#, kde-format
msgid "This field should contain an email address"
msgstr "Ово поље би требало да садржи адресу е‑поште"

#: KCMTelepathyAccounts/validated-line-edit.cpp:129
#, kde-format
msgid "This field should contain a hostname"
msgstr "Ово поље би требало да садржи име домаћина"

#: KCMTelepathyAccounts/validated-line-edit.cpp:133
#, kde-format
msgid "This field should contain an IP address"
msgstr "Ово поље би требало да садржи ИП адресу"

#: KCMTelepathyAccounts/validated-line-edit.cpp:178
#, kde-format
msgid "This field is valid"
msgstr "Ово поље је добро"

# >> @option:check
#. i18n: ectx: property (text), widget (QLabel, salutEnableLabel)
#: main-widget.ui:80
#, kde-format
msgid "Enable local network discovery"
msgstr "Откривање локалне мреже"

#. i18n: ectx: property (text), widget (QLabel, salutEnableStatusLabel)
#: main-widget.ui:100
#, kde-format
msgid "Connect to people near you"
msgstr "Повежите се са људима у близини"

#. i18n: ectx: property (text), widget (KPushButton, addAccountButton)
#: main-widget.ui:151
#, kde-format
msgid "Add Account"
msgstr "Додај налог"

#. i18n: ectx: property (text), widget (KPushButton, editAccountButton)
#: main-widget.ui:161
#, kde-format
msgid "Edit Account"
msgstr "Уреди налог"

#. i18n: ectx: property (toolTip), widget (KPushButton, editAccountIdentityButton)
#: main-widget.ui:171
#, kde-format
msgid "Edit how you appear to others. Requires the account to be online."
msgstr "Уредите како ћете изгледати другима. Захтева да налог буде на вези."

#. i18n: ectx: property (text), widget (KPushButton, editAccountIdentityButton)
#: main-widget.ui:174
#, kde-format
msgid "Edit Identity"
msgstr "Уреди идентитет"

#: salut-message-widget.cpp:50
#, kde-format
msgid "Configure manually..."
msgstr "Подеси ручно..."

#: salut-message-widget.cpp:54
#, kde-format
msgid "Cancel"
msgstr "Одустани"

#: salut-message-widget.cpp:104
#, kde-format
msgid "You will appear as \"%1\" on your local network."
msgstr "Појавићете се као „%1“ на локалној мрежи."
